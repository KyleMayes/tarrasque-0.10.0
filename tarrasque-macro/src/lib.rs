// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Macro implementations for `tarrasque`.

#![recursion_limit="128"]

#[macro_use]
extern crate quote;

extern crate proc_macro;
extern crate proc_macro2;
extern crate syn;

use proc_macro2::{TokenStream};
use quote::{ToTokens};
use syn::*;
use syn::parse::{Parse, ParseStream, Result};
use syn::punctuated::{Punctuated};

/// An extract struct parameter (e.g., `size: i32`).
struct ExtractParam {
    ident: Ident,
    ty: Type,
}

impl Parse for ExtractParam {
    fn parse(input: ParseStream) -> Result<Self> {
        let ident = input.parse()?;
        input.parse::<Token![:]>()?;
        let ty = input.parse()?;
        Ok(ExtractParam { ident, ty })
    }
}

/// An extract struct field expression (e.g., `[]`, `[size]`, or `foo + bar`).
enum ExtractExpr {
    Expr(Expr),
    Extract(Option<Expr>),
}

impl Parse for ExtractExpr {
    fn parse(input: ParseStream) -> Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(token::Bracket) {
            let content;
            bracketed!(content in input);
            if !content.is_empty() {
                Ok(ExtractExpr::Extract(Some(content.parse()?)))
            } else {
                Ok(ExtractExpr::Extract(None))
            }
        } else {
            Ok(ExtractExpr::Expr(input.parse()?))
        }
    }
}

fn parse_ext_impls(input: ParseStream) -> Result<(bool, bool)> {
    let lookahead = input.lookahead1();
    if lookahead.peek(Token![@]) {
        input.parse::<Token![@]>()?;
        match &input.parse::<Ident>()?.to_string()[..] {
            "offset" => Ok((true, false)),
            "size" => Ok((false, true)),
            _ => Ok((false, false)),
        }
    } else {
        Ok((false, false))
    }
}

/// The presence of an extract struct field in the struct.
enum ExtractFieldPresence {
    Field(Visibility),
    Variable,
}

impl Parse for ExtractFieldPresence {
    fn parse(input: ParseStream) -> Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(Token![let]) {
            input.parse::<Token![let]>()?;
            Ok(ExtractFieldPresence::Variable)
        } else {
            Ok(ExtractFieldPresence::Field(input.parse()?))
        }
    }
}

/// An extract struct field (e.g., `pub value: u64 = [size]`).
struct ExtractField {
    attrs: Vec<Attribute>,
    offset: bool,
    size: bool,
    presence: ExtractFieldPresence,
    ident: Ident,
    ty: Type,
    expr: ExtractExpr,
}

impl ExtractField {
    fn include(&self) -> bool {
        match self.presence {
            ExtractFieldPresence::Field(_) => true,
            _ => false,
        }
    }
}

impl Parse for ExtractField {
    fn parse(input: ParseStream) -> Result<Self> {
        let attrs = input.call(Attribute::parse_outer)?;
        let (offset, size) = parse_ext_impls(input)?;
        let presence = input.parse()?;
        let ident = input.parse()?;
        input.parse::<Token![:]>()?;
        let ty = input.parse()?;
        input.parse::<Token![=]>()?;
        let expr = input.parse()?;
        Ok(ExtractField { attrs, offset, size, presence, ident, ty, expr })
    }
}

/// An extract struct.
struct ExtractStruct {
    attrs: Vec<Attribute>,
    vis: Visibility,
    ident: Ident,
    generics: Generics,
    size: Option<Expr>,
    params: Option<Punctuated<ExtractParam, Token![,]>>,
    where_: Option<WhereClause>,
    fields: Punctuated<ExtractField, Token![,]>,
}

impl Parse for ExtractStruct {
    fn parse(input: ParseStream) -> Result<Self> {
        let attrs = input.call(Attribute::parse_outer)?;
        let vis = input.parse()?;
        input.parse::<Token![struct]>()?;
        let ident = input.parse()?;
        let generics = input.parse()?;

        // Parse size if provided.
        let lookahead = input.lookahead1();
        let size = if lookahead.peek(token::Bracket) {
            let content;
            bracketed!(content in input);
            Some(content.parse()?)
        } else {
            None
        };

        // Parse parameters if provided.
        let lookahead = input.lookahead1();
        let params = if lookahead.peek(token::Paren) {
            let content;
            parenthesized!(content in input);
            Some(content.parse_terminated(ExtractParam::parse)?)
        } else {
            None
        };

        // Parse where clause if provided.
        let lookahead = input.lookahead1();
        let where_ = if lookahead.peek(Token![where]) {
            Some(input.parse()?)
        } else {
            None
        };

        let content;
        braced!(content in input);
        let fields = content.parse_terminated(ExtractField::parse)?;

        Ok(ExtractStruct {
            attrs, vis, ident, generics, where_, size, params, fields
        })
    }
}

fn generate_field(field: &ExtractField) -> Option<TokenStream> {
    if let ExtractFieldPresence::Field(ref vis) = field.presence {
        let attrs = &field.attrs;
        let ident = &field.ident;
        let ty = &field.ty;
        Some(quote!(#(#attrs)* #vis #ident: #ty))
    } else {
        None
    }
}

fn generate_struct(struct_: &ExtractStruct) -> TokenStream {
    let attrs = &struct_.attrs;
    let vis = &struct_.vis;
    let ident = &struct_.ident;
    let generics = &struct_.generics;
    let where_ = &struct_.where_;
    let fields = struct_.fields.iter().filter_map(generate_field);
    quote!(#(#attrs)* #vis struct #ident #generics #where_ { #(#fields),* })
}

fn generate_params(struct_: &ExtractStruct) -> (TokenStream, TokenStream) {
    if let Some(params) = &struct_.params {
        let idents = params.iter().map(|a| &a.ident);
        let tys = params.iter().map(|a| &a.ty);
        (quote!((#(#idents),*)), quote!((#(#tys),*)))
    } else {
        (quote!(_), quote!(()))
    }
}

fn generate_expr(field: &ExtractField) -> TokenStream {
    let ident = &field.ident;
    let ty = &field.ty;
    match &field.expr {
        ExtractExpr::Expr(expr) => quote!(let #ident: #ty = #expr;),
        ExtractExpr::Extract(expr) => if let Some(expr) = expr {
            quote!(let #ident: #ty = stream.extract(#expr);)
        } else {
            quote!(let #ident: #ty = stream.extract(());)
        }
    }
}

fn generate_ext_impls(
    struct_: &ExtractStruct,
    ident: impl ToTokens,
    prefix: impl ToTokens,
    suffix: impl ToTokens
) -> TokenStream {
    let mut tokens = TokenStream::new();

    if let Some(size) = &struct_.size {
        tokens.extend(quote!(
            impl #prefix ::tarrasque::Span for #ident #suffix {
                const SPAN: usize = #size;
            }
        ));
    }

    if let Some(field) = struct_.fields.iter().find(|f| f.offset) {
        let field_ident = &field.ident;
        tokens.extend(quote!(
            impl #prefix ::tarrasque::view::Offset for #ident #suffix {
                #[inline]
                fn offset(self) -> usize {
                    self.#field_ident as usize
                }
            }
        ));
    }

    if let Some(field) = struct_.fields.iter().find(|f| f.size) {
        let field_ident = &field.ident;
        tokens.extend(quote!(
            impl #prefix ::tarrasque::view::Record for #ident #suffix {
                #[inline]
                fn size(self) -> usize {
                    self.#field_ident as usize
                }
            }
        ));
    }

    tokens
}

fn generate_impls(struct_: &ExtractStruct) -> TokenStream {
    let params = &struct_.generics.params;
    let clause = &struct_.generics.where_clause;
    let prefix = quote!(<'s, #params> #clause);

    let generics = &struct_.generics;
    let where_ = &struct_.where_;
    let suffix = quote!(#generics #where_);

    let (pat, ty) = generate_params(&struct_);
    let ident = &struct_.ident;
    let extracts = struct_.fields.iter().map(generate_expr);
    let fields = struct_.fields.iter()
        .filter(|f| f.include())
        .map(|f| &f.ident);
    let mut tokens = quote!(
        impl #prefix ::tarrasque::Extract<'s, #ty> for #ident #suffix {
            #[inline]
            fn extract(
                mut stream: &mut ::tarrasque::Stream<'s>, #pat: #ty
            ) -> Self {
                #(#extracts)*
                Self { #(#fields),* }
            }
        }
    );

    tokens.extend(generate_ext_impls(&struct_, ident, &prefix, &suffix));
    tokens
}

fn generate_try_expr(field: &ExtractField) -> TokenStream {
    let ident = &field.ident;
    let ty = &field.ty;
    match &field.expr {
        ExtractExpr::Expr(expr) => quote!(let #ident: #ty = #expr;),
        ExtractExpr::Extract(expr) => if let Some(expr) = expr {
            quote!(let #ident: #ty = stream.try_extract(#expr)?;)
        } else {
            quote!(let #ident: #ty = stream.try_extract(())?;)
        }
    }
}

fn generate_try_impls(struct_: &ExtractStruct) -> TokenStream {
    let params = &struct_.generics.params;
    let clause = &struct_.generics.where_clause;
    let prefix = quote!(<'s, #params> #clause);

    let generics = &struct_.generics;
    let where_ = &struct_.where_;
    let suffix = quote!(#generics #where_);

    let (pat, ty) = generate_params(&struct_);
    let ident = &struct_.ident;
    let extracts = struct_.fields.iter().map(generate_try_expr);
    let fields = struct_.fields.iter()
        .filter(|f| f.include())
        .map(|f| &f.ident);
    let mut tokens = quote!(
        impl #prefix ::tarrasque::TryExtract<'s, #ty> for #ident #suffix {
            #[inline]
            fn try_extract(
                mut stream: &mut ::tarrasque::Stream<'s>, #pat: #ty
            ) -> ::tarrasque::ExtractResult<'s, Self> {
                #(#extracts)*
                Ok(Self { #(#fields),* })
            }
        }
    );

    tokens.extend(generate_ext_impls(&struct_, ident, &prefix, &suffix));
    tokens
}

/// Generates a struct and an implementation of `Extract` (and optionally
/// `Span`, `Offset`, and `Record`) for that struct.
///
/// # Example
///
/// The following code is an example of the usage of the `extract!` macro:
///
/// ```ignore
/// use tarrasque::{Endianness, extract};
///
/// extract! {
///     /// A 2D point.
///     #[derive(Copy, Clone, Debug, PartialEq, Eq)]
///     pub struct Point[4](endianness: Endianness) {
///         /// The x-coordinate of this point.
///         pub x: u16 = [endianness],
///         /// The y-coordinate of this point.
///         pub y: u16 = [endianness],
///     }
/// }
/// # fn main() { }
/// ```
///
/// The `[4]` that follows `Point` is an optional component that statically
/// defines the number of bytes consumed by the generated `Extract`
/// implementation. When this component is present, a `Span` implementation is
/// generated using the specified span.
///
/// The `(endianness: Endianness)` that follows `[4]` is another optional
/// component that defines the parameter used by the `Extract` implementation.
/// In this case, a specified endianness is used to extract the `u16`s instead
/// of using the default (big-endian). When this component is omitted, a
/// placeholder parameter of type `()` is used instead.
///
/// Though not shown here, structs may also make use of generics and lifetimes.
/// See below for an example.
///
/// Each field has a corresponding expression which is used to populate it in
/// the `Extract` implementation. Square brackets indicate that the field is to
/// be populated by extracting a value of that type from the stream. The
/// expression in the square brackets is used as the argument for the `extract`
/// method. If the square brackets do not contain an expression, `()` is used as
/// the argument.
///
/// Expressions not wrapped in square brackets are treated as normal
/// expressions. The `[endianness]` expression could be replaced with the
/// equivalent `stream.extract(endianness)`. Note the usage of `stream` which
/// is implicitly available in these expressions.
///
/// ## Expansion
///
/// The usage of `extract!` shown above generates the following code:
///
/// ```ignore
/// use tarrasque::{Endianness, extract};
///
/// /// A 2D point.
/// #[derive(Copy, Clone, Debug, PartialEq, Eq)]
/// pub struct Point {
///     /// The x-coordinate of this point.
///     pub x: u16,
///     /// The y-coordinate of this point.
///     pub y: u16,
/// }
///
/// impl<'s> ::tarrasque::Extract<'s, Endianness> for Point {
///     #[inline]
///     fn extract(
///         mut stream: &mut ::tarrasque::Stream<'s>, endianness: Endianness
///     ) -> Self {
///         let x = stream.extract(endianness);
///         let y = stream.extract(endianness);
///         Point { x, y }
///     }
/// }
///
/// impl ::tarrasque::Span for Point {
///     const SPAN: usize = 4;
/// }
/// ```
///
/// ## Generics and Lifetimes
///
/// The following code is an example of the usage of the `extract!` macro with
/// generics and lifetimes.
///
/// ```ignore
/// use tarrasque::{Extract, Span, extract};
///
/// extract! {
///     /// A pair of values.
///     pub struct Pair<'s, T>[2 * T::SPAN] where T: Extract<'s, ()> + Span {
///         pub bytes: &'s [u8] = &stream[..],
///         pub left: T = [],
///         pub right: T = [],
///     }
/// }
/// # fn main() { }
/// ```
///
/// **Note:** As shown above, the lifetime `'s` must be used to refer to the
/// lifetime of the stream.
///
/// ## Temporary Values
///
/// In the below example, `a` and `b` are temporary bindings that are used to
/// extract instances of `Let` but are not included in the `Let` struct. The
/// expressions used to populate these temporary bindings are identical to those
/// used to populate fields.
///
/// ```ignore
/// use tarrasque::{extract};
///
/// extract! {
///     pub struct Let[4] {
///         let a: u16 = [],
///         let b: u16 = [],
///         pub c: u16 = a + b,
///     }
/// }
/// # fn main() { }
/// ```
#[proc_macro]
pub fn extract(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let struct_ = parse_macro_input!(input as ExtractStruct);

    let mut tokens = TokenStream::new();
    tokens.extend(generate_struct(&struct_));
    tokens.extend(generate_impls(&struct_));

    tokens.into()
}

/// Generates a struct and an implementation of `TryExtract` (and optionally
/// `Span`, `Offset`, and `Record`) for that struct.
///
/// Functions identically to `extract!` other than generating `TryExtract`
/// instead of `Extract`.
#[proc_macro]
pub fn try_extract(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let struct_ = parse_macro_input!(input as ExtractStruct);

    let mut tokens = TokenStream::new();
    tokens.extend(generate_struct(&struct_));
    tokens.extend(generate_try_impls(&struct_));

    tokens.into()
}
