# tarrasque

[![crates.io](https://img.shields.io/crates/v/tarrasque.svg)](https://crates.io/crates/tarrasque)
[![docs.rs](https://docs.rs/tarrasque/badge.svg)](https://docs.rs/tarrasque)
[![travis-ci.com](https://travis-ci.org/glyph-rs/tarrasque.svg?branch=master)](https://travis-ci.org/glyph-rs/tarrasque)

A library for zero-allocation parsing of binary formats.

Released under the Apache License 2.0.

Supported on Rust 1.31.0 and later.

## Example

```rust
use tarrasque::{Endianness, ExtractError, Stream, extract};

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct Point[4](endianness: Endianness) {
        /// The x-coordinate of this point.
        pub x: u16 = [endianness],
        /// The y-coordinate of this point.
        pub y: u16 = [endianness],
    }
}

fn main() {
    // A stream of bytes.
    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);

    // Extract a point containing two big-endian `u16`s from the stream.
    let point = stream.extract::<Point, _>(Endianness::Big);
    assert_eq!(point, Point { x: 258, y: 772 });

    // Extract a point containing two little-endian `u16`s from the stream.
    let point = stream.extract::<Point, _>(Endianness::Little);
    assert_eq!(point, Point { x: 1541, y: 2055 });

    // Attempt to extract a point from the empty stream.
    let point = stream.try_extract::<Point, _>(Endianness::Big);
    assert_eq!(point, Err(ExtractError::Insufficient(4)));
}
```
