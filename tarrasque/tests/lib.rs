// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use tarrasque::*;
use tarrasque::view::*;
use tarrasque::Endianness::*;

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointA {
        /// The x-coordinate of this point.
        pub x: u16 = [],
        /// The y-coordinate of this point.
        pub y: u16 = [],
    }
}

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointB[4] {
        /// The x-coordinate of this point.
        pub x: u16 = [],
        /// The y-coordinate of this point.
        pub y: u16 = [],
    }
}

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointC(endianness: Endianness) {
        /// The x-coordinate of this point.
        pub x: u16 = [endianness],
        /// The y-coordinate of this point.
        pub y: u16 = [endianness],
    }
}

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointD[4](endianness: Endianness) {
        /// The x-coordinate of this point.
        pub x: u16 = [endianness],
        /// The y-coordinate of this point.
        pub y: u16 = [endianness],
    }
}

#[test]
fn test_extract_point() {
    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.extract::<PointA, _>(()), PointA { x: 258, y: 772 });
    assert_eq!(stream.extract::<PointA, _>(()), PointA { x: 1286, y: 1800 });

    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.extract::<PointB, _>(()), PointB { x: 258, y: 772 });
    assert_eq!(stream.extract::<PointB, _>(()), PointB { x: 1286, y: 1800 });
    assert_eq!(PointB::SPAN, 4);

    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.extract::<PointC, _>(Big), PointC { x: 258, y: 772 });
    assert_eq!(stream.extract::<PointC, _>(Little), PointC { x: 1541, y: 2055 });

    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.extract::<PointD, _>(Big), PointD { x: 258, y: 772 });
    assert_eq!(stream.extract::<PointD, _>(Little), PointD { x: 1541, y: 2055 });
    assert_eq!(PointD::SPAN, 4);
}

extract! {
    /// A list of values.
    #[derive(Copy, Clone, Debug)]
    pub struct List<'s, T, P>(parameter: P) where T: Extract<'s, P> + Span, P: Copy {
        /// The bytes in this list.
        pub bytes: &'s [u8] = &stream[..],
        /// The number of values in this list.
        pub size: u8 = [],
        /// The values in this list.
        pub values: StaticView<'s, T, P> = [(size as usize, parameter)],
    }
}

#[test]
fn test_extract_list() {
    let mut stream = Stream(&[2, 1, 2, 3, 4, 5, 6, 7, 8]);
    let list = stream.extract::<List<PointB, _>, _>(());
    assert_eq!(list.bytes, &[2, 1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(list.size, 2);
    assert_eq!(list.values.iter().collect::<Vec<_>>(), &[
        PointB { x: 258, y: 772 },
        PointB { x: 1286, y: 1800 },
    ]);

    let mut stream = Stream(&[2, 1, 2, 3, 4, 5, 6, 7, 8]);
    let list = stream.extract::<List<PointD, _>, _>(Little);
    assert_eq!(list.bytes, &[2, 1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(list.size, 2);
    assert_eq!(list.values.iter().collect::<Vec<_>>(), &[
        PointD { x: 513, y: 1027 },
        PointD { x: 1541, y: 2055 },
    ]);
}

extract! {
    /// An array of four bytes.
    #[derive(Debug, PartialEq, Eq)]
    pub struct Array<'s>[4] {
        /// The bytes in this array.
        pub bytes: &'s [u8] = [4],
        /// The first byte in this array.
        pub first: u8 = bytes[0],
    }
}

#[test]
fn test_extract_array() {
    let mut stream = Stream(&[1, 2, 3, 4]);
    let array = stream.extract::<Array, _>(());
    assert_eq!(array.bytes, &[1, 2, 3, 4]);
    assert_eq!(array.first, 1);
}

try_extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointE {
        /// The x-coordinate of this point.
        pub x: u16 = [],
        /// The y-coordinate of this point.
        pub y: u16 = [],
    }
}

try_extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointF[4] {
        /// The x-coordinate of this point.
        pub x: u16 = [],
        /// The y-coordinate of this point.
        pub y: u16 = [],
    }
}

try_extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointG(endianness: Endianness) {
        /// The x-coordinate of this point.
        pub x: u16 = [endianness],
        /// The y-coordinate of this point.
        pub y: u16 = [endianness],
    }
}

try_extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct PointH[4](endianness: Endianness) {
        /// The x-coordinate of this point.
        pub x: u16 = [endianness],
        /// The y-coordinate of this point.
        pub y: u16 = [endianness],
    }
}

#[test]
fn test_try_extract_point() {
    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.try_extract::<PointE, _>(()), Ok(PointE { x: 258, y: 772 }));
    assert_eq!(stream.try_extract::<PointE, _>(()), Ok(PointE { x: 1286, y: 1800 }));

    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.try_extract::<PointF, _>(()), Ok(PointF { x: 258, y: 772 }));
    assert_eq!(stream.try_extract::<PointF, _>(()), Ok(PointF { x: 1286, y: 1800 }));
    assert_eq!(PointF::SPAN, 4);

    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.try_extract::<PointG, _>(Big), Ok(PointG { x: 258, y: 772 }));
    assert_eq!(stream.try_extract::<PointG, _>(Little), Ok(PointG { x: 1541, y: 2055 }));

    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
    assert_eq!(stream.try_extract::<PointH, _>(Big), Ok(PointH { x: 258, y: 772 }));
    assert_eq!(stream.try_extract::<PointH, _>(Little), Ok(PointH { x: 1541, y: 2055 }));
    assert_eq!(PointH::SPAN, 4);
}

extract! {
    pub struct Let[1] {
        let a: u8 = 2,
        let b: u8 = [],
        c: u8 = a + b,
    }
}

#[test]
fn test_extract_let() {
    let mut stream = Stream(&[2]);
    assert_eq!(stream.extract::<Let, _>(()).c, 4);
}
