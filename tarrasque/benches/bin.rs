// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate microbench;
extern crate tarrasque;

use core::iter;

use microbench::{Options, retain};

use tarrasque::*;
use tarrasque::view::*;

fn bench_extract_array() {
    let bytes = (0..64).collect::<Vec<_>>();
    let options = Options::default();
    microbench::bench(&options, "extract_array", || {
        Stream(&bytes).extract::<[u16; 32], _>(())
    });
}

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct Point[4] {
        /// The x-coordinate of this point.
        pub x: u16 = [],
        /// The y-coordinate of this point.
        pub y: u16 = [],
    }
}

fn bench_extract_point() {
    let bytes = vec![2, 4, 6, 8];
    let options = Options::default();
    microbench::bench(&options, "extract_point", || {
        Stream(&bytes).extract::<Point, _>(())
    });
}

fn bench_extract_static_view() {
    let bytes = iter::repeat(4).cycle().take(128).collect::<Vec<_>>();
    let options = Options::default();
    microbench::bench(&options, "extract_static_view", || {
        let view: StaticView<Point, ()> = Stream(&bytes).extract((32, ()));
        for point in view.iter() {
            retain(point);
        }
    });
}

fn bench_extract_dynamic_view() {
    let bytes = iter::repeat(4).cycle().take(128).collect::<Vec<_>>();
    let options = Options::default();
    microbench::bench(&options, "extract_dynamic_view", || {
        let view: DynamicView<Point, ()> = Stream(&bytes).extract((4, 32, ()));
        for point in view.iter() {
            retain(point);
        }
    });
}

fn bench_offset_view() {
    let bytes = (0..).step_by(4).take(32).collect::<Vec<_>>();
    let offsets: StaticView<u8, _> = Stream(&bytes[..]).extract((32, ()));
    let bytes = iter::repeat(4).cycle().take(128).collect::<Vec<u8>>();

    let options = Options::default();

    let view: OffsetView<_, Point, _> =
        OffsetView::new(offsets, &bytes[..], ());
    microbench::bench(&options, "offset_view", || {
        for point in view.iter() {
            retain(point);
        }
    });

    let view: DynamicOffsetView<_, Point, _> =
        DynamicOffsetView::new(offsets, &bytes[..], 4, ());
    microbench::bench(&options, "dynamic_offset_view", || {
        for point in view.iter() {
            retain(point);
        }
    });

    let view: StaticOffsetView<_, Point, _> =
        StaticOffsetView::new(offsets, &bytes[..], ());
    microbench::bench(&options, "static_offset_view", || {
        for point in view.iter() {
            retain(point);
        }
    });
}

fn main() {
    bench_extract_array();
    bench_extract_point();
    bench_extract_static_view();
    bench_extract_dynamic_view();
    bench_offset_view();
}
