// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use tarrasque::{Endianness, ExtractError, Stream, extract};

extract! {
    /// A 2D point.
    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub struct Point[4](endianness: Endianness) {
        /// The x-coordinate of this point.
        pub x: u16 = [endianness],
        /// The y-coordinate of this point.
        pub y: u16 = [endianness],
    }
}

fn main() {
    // A stream of bytes.
    let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);

    // Extract a point containing two big-endian `u16`s from the stream.
    let point = stream.extract::<Point, _>(Endianness::Big);
    assert_eq!(point, Point { x: 258, y: 772 });

    // Extract a point containing two little-endian `u16`s from the stream.
    let point = stream.extract::<Point, _>(Endianness::Little);
    assert_eq!(point, Point { x: 1541, y: 2055 });

    // Attempt to extract a point from the empty stream.
    let point = stream.try_extract::<Point, _>(Endianness::Big);
    assert_eq!(point, Err(ExtractError::Insufficient(4)));
}
