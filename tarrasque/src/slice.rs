// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Slice extraction.

use core::str;

use super::{Extract, ExtractError, ExtractResult, Stream, TryExtract};

impl<'s> Extract<'s, ()> for &'s [u8] {
    #[inline]
    fn extract(stream: &mut Stream<'s>, _: ()) -> Self {
        let bytes = &stream[..];
        stream.skip(stream.len());
        bytes
    }
}

impl<'s> TryExtract<'s, ()> for &'s [u8] {
    #[inline]
    fn try_extract(stream: &mut Stream<'s>, _: ()) -> ExtractResult<'s, Self> {
        Ok(stream.extract(()))
    }
}

impl<'s> Extract<'s, usize> for &'s [u8] {
    #[inline]
    fn extract(stream: &mut Stream<'s>, length: usize) -> Self {
        assert!(stream.len() >= length, "insufficient bytes");
        let bytes = &stream[..length];
        stream.skip(length);
        bytes
    }
}

impl<'s> TryExtract<'s, usize> for &'s [u8] {
    #[inline]
    fn try_extract(
        stream: &mut Stream<'s>, length: usize
    ) -> ExtractResult<'s, Self> {
        if stream.len() >= length {
            Ok(stream.extract(length))
        } else {
            Err(ExtractError::Insufficient(length))
        }
    }
}

impl<'s> TryExtract<'s, ()> for &'s str {
    #[inline]
    fn try_extract(stream: &mut Stream<'s>, _: ()) -> ExtractResult<'s, Self> {
        stream.try_extract(stream.len())
    }
}

impl<'s> TryExtract<'s, usize> for &'s str {
    #[inline]
    fn try_extract(
        stream: &mut Stream<'s>, length: usize
    ) -> ExtractResult<'s, Self> {
        let bytes = stream.try_extract(length)?;
        str::from_utf8(bytes).map_err(|e| ExtractError::Encoding(bytes, e))
    }
}

#[cfg(test)]
mod test {
    use crate::*;
    use crate::ExtractError::*;

    #[test]
    fn test_extract_slice() {
        let mut stream = Stream(&[1, 2, 3, 4]);
        assert_eq!(stream.extract::<&[u8], _>(()), &[1, 2, 3, 4]);
        assert_eq!(stream.extract::<&[u8], _>(()), &[]);

        let mut stream = Stream(&[1, 2, 3, 4]);
        assert_eq!(stream.extract::<&[u8], _>(2), &[1, 2]);
        assert_eq!(stream.extract::<&[u8], _>(2), &[3, 4]);
    }

    #[test]
    #[should_panic(expected="insufficient bytes")]
    fn test_extract_slice_panic() {
        let mut stream = Stream(&[1, 2, 3, 4]);
        let _ = stream.extract::<&[u8], _>(5);
    }

    #[test]
    fn test_try_extract_slice() {
        let mut stream = Stream(&[1, 2, 3, 4]);
        assert_eq!(stream.try_extract::<&[u8], _>(()), Ok(&[1, 2, 3, 4][..]));
        assert_eq!(stream.try_extract::<&[u8], _>(()), Ok(&[][..]));

        let mut stream = Stream(&[1, 2, 3, 4]);
        assert_eq!(stream.try_extract::<&[u8], _>(2), Ok(&[1, 2][..]));
        assert_eq!(stream.try_extract::<&[u8], _>(2), Ok(&[3, 4][..]));
        assert_eq!(stream.try_extract::<&[u8], _>(2), Err(Insufficient(2)));
    }

    #[test]
    fn test_try_extract_str() {
        let mut stream = Stream(&[b'a', b's', b'd', b'f']);
        assert_eq!(stream.try_extract::<&str, _>(()), Ok("asdf"));
        assert_eq!(stream.try_extract::<&str, _>(()), Ok(""));

        let mut stream = Stream(&[b'a', b's', 0xC3, 0x28]);
        match stream.try_extract::<&str, _>(()) {
            Err(Encoding(bytes, error)) => {
                assert_eq!(bytes, &[b'a', b's', 0xC3, 0x28]);
                assert_eq!(error.valid_up_to(), 2);
            },
            _ => unreachable!(),
        }

        let mut stream = Stream(&[b'a', b's', b'd', b'f']);
        assert_eq!(stream.try_extract::<&str, _>(2), Ok("as"));
        assert_eq!(stream.try_extract::<&str, _>(2), Ok("df"));
        assert_eq!(stream.try_extract::<&str, _>(2), Err(Insufficient(2)));

        let mut stream = Stream(&[b'a', b's', 0xC3, 0x28]);
        assert_eq!(stream.try_extract::<&str, _>(2), Ok("as"));
        match stream.try_extract::<&str, _>(2) {
            Err(Encoding(bytes, error)) => {
                assert_eq!(bytes, &[0xC3, 0x28]);
                assert_eq!(error.valid_up_to(), 0);
            },
            _ => unreachable!(),
        }
    }
}
