// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! A library for zero-allocation parsing of binary formats.
//!
//! # Example
//!
//! ```
//! use tarrasque::{Endianness, ExtractError, Stream, extract};
//!
//! extract! {
//!     /// A 2D point.
//!     #[derive(Copy, Clone, Debug, PartialEq, Eq)]
//!     pub struct Point[4](endianness: Endianness) {
//!         /// The x-coordinate of this point.
//!         pub x: u16 = [endianness],
//!         /// The y-coordinate of this point.
//!         pub y: u16 = [endianness],
//!     }
//! }
//!
//! fn main() {
//!     // A stream of bytes.
//!     let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
//!
//!     // Extract a point containing two big-endian `u16`s from the stream.
//!     let point = stream.extract::<Point, _>(Endianness::Big);
//!     assert_eq!(point, Point { x: 258, y: 772 });
//!
//!     // Extract a point containing two little-endian `u16`s from the stream.
//!     let point = stream.extract::<Point, _>(Endianness::Little);
//!     assert_eq!(point, Point { x: 1541, y: 2055 });
//!
//!     // Attempt to extract a point from the empty stream.
//!     let point = stream.try_extract::<Point, _>(Endianness::Big);
//!     assert_eq!(point, Err(ExtractError::Insufficient(4)));
//! }
//! ```

#![deny(missing_copy_implementations, missing_debug_implementations, missing_docs)]

#![no_std]

mod array;
mod number;
mod slice;
pub mod view;

use core::ops::{Deref, RangeBounds};
use core::str::{Utf8Error};

pub use tarrasque_macro::{extract, try_extract};

pub use crate::array::*;
pub use crate::number::*;
pub use crate::slice::*;

/// A type that can infallibly be extracted from a stream of bytes.
pub trait Extract<'s, P> {
    /// Extracts a value of this type from the supplied stream of bytes.
    ///
    /// See [`Stream::extract`] for usage information.
    ///
    /// # Panics
    ///
    /// * `stream` has insufficient bytes
    fn extract(stream: &mut Stream<'s>, _: P) -> Self where Self: Sized;
}

/// An error encountered while extracting a value from a stream of bytes.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ExtractError<'s> {
    /// An error code.
    Code(u32),
    /// The stream contained an invalid UTF-8 string.
    Encoding(&'s [u8], Utf8Error),
    /// The stream did not contain enough bytes to extract the value.
    Insufficient(usize),
}

/// The result of extracting a value from a stream of bytes.
pub type ExtractResult<'s, T> = Result<T, ExtractError<'s>>;

/// A type that can fallibly be extracted from a stream of bytes.
pub trait TryExtract<'s, P> {
    /// Extracts a value of this type from the supplied stream of bytes.
    ///
    /// See [`Stream::try_extract`] for usage information.
    fn try_extract(
        stream: &mut Stream<'s>, _: P
    ) -> ExtractResult<'s, Self> where Self: Sized;
}

impl<'s, P, T> TryExtract<'s, P> for T where T: Extract<'s, P> + Span {
    #[inline]
    fn try_extract(
        stream: &mut Stream<'s>, parameter: P
    ) -> ExtractResult<'s, Self> {
        if stream.len() >= T::SPAN {
            Ok(stream.extract(parameter))
        } else {
            Err(ExtractError::Insufficient(T::SPAN))
        }
    }
}

/// An extractable type that spans a fixed number of bytes.
pub trait Span {
    /// The number of bytes spanned by values of this type.
    const SPAN: usize;
}

/// A stream of bytes from which values can be extracted.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Stream<'s>(pub &'s [u8]);

impl<'s> Stream<'s> {
    /// Extracts bytes from this stream as a `T`.
    ///
    /// # Panics
    ///
    /// * insufficient bytes
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// let mut stream = Stream(&[1, 2]);
    /// assert_eq!(stream.extract::<u8, _>(()), 1);
    /// assert_eq!(stream.extract::<u8, _>(()), 2);
    /// ```
    #[inline]
    pub fn extract<T, P>(&mut self, parameter: P) -> T where T: Extract<'s, P> {
        T::extract(self, parameter)
    }

    /// Extracts bytes from this stream as a `T`.
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// # use tarrasque::ExtractError::*;
    /// let mut stream = Stream(&[1, 2]);
    /// assert_eq!(stream.try_extract::<u8, _>(()), Ok(1));
    /// assert_eq!(stream.try_extract::<u8, _>(()), Ok(2));
    /// assert_eq!(stream.try_extract::<u8, _>(()), Err(Insufficient(1)));
    /// ```
    #[inline]
    pub fn try_extract<T, P>(
        &mut self, parameter: P
    ) -> ExtractResult<'s, T> where T: TryExtract<'s, P> {
        T::try_extract(self, parameter)
    }

    /// Returns bytes from this stream as a `T` without extracting the bytes.
    ///
    /// # Panics
    ///
    /// * insufficient bytes
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// let mut stream = Stream(&[1, 2, 3, 4]);
    /// assert_eq!(stream.peek::<u16, _>(()), 0x0102);
    /// assert_eq!(stream.extract::<u16, _>(()), 0x0102);
    /// assert_eq!(stream.peek::<u16, _>(()), 0x0304);
    /// assert_eq!(stream.extract::<u16, _>(()), 0x0304);
    /// ```
    #[inline]
    pub fn peek<T, P>(&mut self, parameter: P) -> T where T: Extract<'s, P> {
        T::extract(&mut Stream(self.0), parameter)
    }

    /// Returns bytes from this stream as a `T` without extracting the bytes.
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// # use tarrasque::ExtractError::*;
    /// let mut stream = Stream(&[1, 2, 3, 4]);
    /// assert_eq!(stream.try_peek::<u16, _>(()), Ok(0x0102));
    /// assert_eq!(stream.try_extract::<u16, _>(()), Ok(0x0102));
    /// assert_eq!(stream.try_peek::<u16, _>(()), Ok(0x0304));
    /// assert_eq!(stream.try_extract::<u16, _>(()), Ok(0x0304));
    /// assert_eq!(stream.try_peek::<u16, _>(()), Err(Insufficient(2)));
    /// assert_eq!(stream.try_extract::<u16, _>(()), Err(Insufficient(2)));
    /// ```
    #[inline]
    pub fn try_peek<T, P>(
        &mut self, parameter: P
    ) -> ExtractResult<'s, T> where T: TryExtract<'s, P> {
        T::try_extract(&mut Stream(self.0), parameter)
    }

    /// Skips the supplied number of bytes in this stream.
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// let mut stream = Stream(&[1, 2, 3, 4]);
    /// stream.skip(2);
    /// assert_eq!(stream.extract::<u16, _>(()), 0x0304);
    /// ```
    #[inline]
    pub fn skip(&mut self, n: usize) {
        if self.0.len() > n {
            self.0 = &self.0[n..];
        } else {
            self.0 = &[];
        }
    }

    /// Skips bytes in this stream while the supplied condition is true.
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// let mut stream = Stream(&[1, 2, 3, 4]);
    /// stream.skip_while(|b| b < 3);
    /// assert_eq!(stream.extract::<u16, _>(()), 0x0304);
    /// ```
    #[inline]
    pub fn skip_while(&mut self, f: impl Fn(u8) -> bool) {
        while !self.0.is_empty() && f(self.0[0]) {
            self.0 = &self.0[1..];
        }
    }

    /// Returns the substream in this stream corresponding to the supplied range.
    ///
    /// # Example
    ///
    /// ```
    /// # use tarrasque::{Stream};
    /// # use tarrasque::ExtractError::*;
    /// let stream = Stream(&[1, 2, 3, 4]);
    /// assert_eq!(stream.substream(1..3), Ok(Stream(&[2, 3])));
    /// assert_eq!(stream.substream(4..), Err(Insufficient(5)));
    /// assert_eq!(stream.substream(..=4), Err(Insufficient(5)));
    /// ```
    #[inline]
    pub fn substream(
        &self, bounds: impl RangeBounds<usize>
    ) -> ExtractResult<'s, Stream<'s>> {
        use core::ops::Bound::*;

        if let Included(start) = bounds.start_bound() {
            if *start >= self.0.len() {
                return Err(ExtractError::Insufficient(*start + 1));
            }
        }

        match bounds.end_bound() {
            Included(end) if *end >= self.0.len() =>
                return Err(ExtractError::Insufficient(*end + 1)),
            Excluded(end) if *end > self.0.len() =>
                return Err(ExtractError::Insufficient(*end)),
            _ => { },
        }

        match (bounds.start_bound(), bounds.end_bound()) {
            (Included(start), Included(end)) => Ok(Stream(&self.0[*start..=*end])),
            (Included(start), Excluded(end)) => Ok(Stream(&self.0[*start..*end])),
            (Included(start), Unbounded) => Ok(Stream(&self.0[*start..])),
            (Unbounded, Included(end)) => Ok(Stream(&self.0[..=*end])),
            (Unbounded, Excluded(end)) => Ok(Stream(&self.0[..*end])),
            (Unbounded, Unbounded) => Ok(*self),
            _ => unreachable!(),
        }
    }
}

impl<'s> Deref for Stream<'s> {
    type Target = &'s [u8];

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
