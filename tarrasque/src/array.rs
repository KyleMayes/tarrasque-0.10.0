// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Array extraction.

use super::{Extract, Span, Stream};

macro_rules! extract {
    ($($size:expr), *,) => {
        $(impl<'s, T, P> Extract<'s, P> for [T; $size] where
            T: Extract<'s, P> + Default + Copy,
            P: Copy
        {
            #[inline]
            fn extract(stream: &mut Stream<'s>, parameter: P) -> Self {
                let mut value = [T::default(); $size];

                for slot in &mut value {
                    *slot = stream.extract(parameter);
                }

                value
            }
        }

        impl<'s, T> Span for [T; $size] where T: Span {
            const SPAN: usize = T::SPAN * $size;
        })*
    };
}

extract!(
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
    17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
);

#[cfg(test)]
mod test {
    use crate::*;
    use crate::Endianness::*;
    use crate::ExtractError::*;

    #[test]
    fn test_extract_array() {
        let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
        let view: [u16; 4] = stream.extract(());
        assert_eq!(view[0], 0x0102);
        assert_eq!(view[1], 0x0304);
        assert_eq!(view[2], 0x0506);
        assert_eq!(view[3], 0x0708);

        let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
        let view: [u16; 4] = stream.extract(Little);
        assert_eq!(view[0], 0x0201);
        assert_eq!(view[1], 0x0403);
        assert_eq!(view[2], 0x0605);
        assert_eq!(view[3], 0x0807);
    }

    #[test]
    #[should_panic(expected="insufficient bytes")]
    fn test_extract_array_panic() {
        let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
        let _ = stream.extract::<[u16; 32], _>(());
    }

    #[test]
    fn test_try_extract_array() {
        let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
        let view: [u16; 4] = stream.try_extract(()).unwrap();
        assert_eq!(view[0], 0x0102);
        assert_eq!(view[1], 0x0304);
        assert_eq!(view[2], 0x0506);
        assert_eq!(view[3], 0x0708);

        let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
        let view: [u16; 4] = stream.try_extract(Little).unwrap();
        assert_eq!(view[0], 0x0201);
        assert_eq!(view[1], 0x0403);
        assert_eq!(view[2], 0x0605);
        assert_eq!(view[3], 0x0807);

        let mut stream = Stream(&[1, 2, 3, 4, 5, 6, 7, 8]);
        assert_eq!(stream.try_extract::<[u16; 32], _>(()), Err(Insufficient(64)));
    }
}
