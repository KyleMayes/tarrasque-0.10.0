// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Record views.

use core::marker::{PhantomData};

use crate::{Extract, TryExtract};
use super::{Offset, View};

/// A type usable as a record.
pub trait Record: Offset {
    /// Returns this value as a value size.
    fn size(self) -> usize;
}

/// A sequence of values at byte offsets where the sizes of the values and the
/// byte offsets are defined by records.
///
/// This view differs from `ParameterRecordView` in that the records are not
/// used as parameters when extracting values.
///
/// # Example
///
/// ```
/// # use tarrasque::{Endianness, Extract, Stream, extract, try_extract};
/// # use tarrasque::view::{Offset, Record, RecordView, StaticView, View};
/// extract! {
///     /// A list record.
///     #[derive(Copy, Clone, Debug)]
///     struct ListRecord[2] {
///         @offset offset: u8 = [],
///         @size size: u8 = [],
///     }
/// }
///
/// try_extract! {
///     /// A list of `u16`s.
///     #[derive(Copy, Clone, Debug)]
///     struct List<'s>(endianness: Endianness) {
///         numbers: StaticView<'s, u16, Endianness> =
///             [((stream.len() / 2) as usize, endianness)],
///     }
/// }
///
/// # fn main() {
/// // Extract a view of records.
/// let bytes = &[0, 2, 4, 4, 10, 2];
/// let records: StaticView<ListRecord, _> = Stream(bytes).extract((3, ()));
///
/// // Construct a view of 3 lists of little-endian `u16`s defined by above records.
/// let bytes = &[1, 2, 0, 0, 3, 4, 5, 6, 0, 0, 7, 8];
/// let view: RecordView<_, List, _> =
///     RecordView::new(records, bytes, Endianness::Little);
///
/// // Extract the bytes for the values in the view.
/// // Note that the number of bytes returned corresponds to the size defined by
/// // the records.
/// assert_eq!(view.get(0), Some(&[1, 2][..]));
/// assert_eq!(view.get(1), Some(&[3, 4, 5, 6][..]));
/// assert_eq!(view.get(2), Some(&[7, 8][..]));
/// assert_eq!(view.get(3), None);
///
/// // Extract a values in the view.
/// let list = view.try_extract(1).unwrap().unwrap();
/// assert_eq!(list.numbers.iter().collect::<Vec<_>>(), &[0x0403, 0x0605]);
/// # }
/// ```
#[derive(Copy, Clone, Debug)]
pub struct RecordView<'s, R, T, P> where
    R: View<'s> + Copy,
    R::Item: Extract<'s, R::Parameter> + Record,
    T: TryExtract<'s, P>,
    P: Copy
{
    records: R,
    bytes: &'s [u8],
    parameter: P,
    _marker: PhantomData<*const T>,
}

impl<'s, R, T, P> RecordView<'s, R, T, P> where
    R: View<'s> + Copy,
    R::Item: Extract<'s, R::Parameter> + Record,
    T: TryExtract<'s, P>,
    P: Copy
{
    /// Returns a new record view.
    #[inline]
    pub fn new(records: R, bytes: &'s [u8], parameter: P) -> Self {
        Self { records, bytes, parameter, _marker: PhantomData }
    }

    /// Returns the records in this record view.
    #[inline]
    pub fn records(&self) -> &R {
        &self.records
    }
}

impl<'s, R, T, P> View<'s> for RecordView<'s, R, T, P> where
    R: View<'s> + Copy,
    R::Item: Extract<'s, R::Parameter> + Record,
    T: TryExtract<'s, P>,
    P: Copy
{
    type Parameter = P;
    type Item = T;

    #[inline]
    fn len(&self) -> usize {
        self.records.len()
    }

    #[inline]
    fn parameter(&self) -> Self::Parameter {
        self.parameter
    }

    #[inline]
    fn get(&self, index: usize) -> Option<&'s [u8]> {
        let record = self.records.extract(index)?;
        let start = record.offset();
        let end = start + record.size();
        if end <= self.bytes.len() {
            Some(&self.bytes[start..end])
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use crate::*;
    use super::*;
    use super::super::*;

    #[derive(Copy, Clone, Debug)]
    struct ListRecord {
        offset: u8,
        size: u8,
    }

    impl<'s> Extract<'s, ()> for ListRecord {
        fn extract(stream: &mut Stream<'s>, _: ()) -> Self {
            let offset = stream.extract(());
            let size = stream.extract(());
            Self { offset, size }
        }
    }

    impl Span for ListRecord {
        const SPAN: usize = 2;
    }

    impl Offset for ListRecord {
        fn offset(self) -> usize {
            self.offset as usize
        }
    }

    impl Record for ListRecord {
        fn size(self) -> usize {
            self.size as usize
        }
    }

    #[derive(Copy, Clone, Debug)]
    struct List<'s> {
        numbers: StaticView<'s, u16, ()>,
    }

    impl<'s> TryExtract<'s, ()> for List<'s> {
        fn try_extract(
            stream: &mut Stream<'s>, _: ()
        ) -> ExtractResult<'s, Self> {
            let size = (stream.len() / 2) as usize;
            Ok(Self { numbers: stream.try_extract((size, ()))? })
        }
    }

    #[test]
    fn test_offset_view() {
        let bytes = &[0, 2, 4, 4, 10, 2];
        let records: StaticView<ListRecord, _> = Stream(bytes).extract((3, ()));
        let bytes = &[1, 2, 0, 0, 3, 4, 5, 6, 0, 0, 7, 8];
        let view: RecordView<_, List, _> = RecordView::new(records, bytes, ());

        assert_eq!(view.get(0), Some(&[1, 2][..]));
        assert_eq!(view.get(1), Some(&[3, 4, 5, 6][..]));
        assert_eq!(view.get(2), Some(&[7, 8][..]));
        assert_eq!(view.get(3), None);

        let list = view.try_extract(0).unwrap().unwrap();
        assert_eq!(list.numbers.extract(0), Some(0x0102));
        assert_eq!(list.numbers.extract(1), None);

        let list = view.try_extract(1).unwrap().unwrap();
        assert_eq!(list.numbers.extract(0), Some(0x0304));
        assert_eq!(list.numbers.extract(1), Some(0x0506));
        assert_eq!(list.numbers.extract(2), None);

        let list = view.try_extract(2).unwrap().unwrap();
        assert_eq!(list.numbers.extract(0), Some(0x0708));
        assert_eq!(list.numbers.extract(1), None);

        assert!(view.try_extract(3).is_none());
    }
}
