// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Offset views.

use core::marker::{PhantomData};

use crate::{Extract, TryExtract};
use super::{View};

/// A type usable as a byte offset.
pub trait Offset: Copy {
    /// Returns this value as a byte offset.
    fn offset(self) -> usize;
}

impl<T> Offset for T where T: Copy + Into<usize> {
    #[inline]
    fn offset(self) -> usize {
        self.into()
    }
}

/// A sequence of values at byte offsets where the size of the values is
/// unknowable in advance or unimportant.
///
/// **Note:** Because the size of the values is unknown, each value will be
/// extracted from the entire subsequence of bytes starting from the offset for
/// that value. If this is a problem, try using either `DynamicOffsetView` or
/// `StaticOffsetView`.
///
/// # Example
///
/// ```
/// # use tarrasque::{Endianness, Extract, Stream};
/// # use tarrasque::view::{OffsetView, StaticView, View};
/// // Extract a view of byte offsets.
/// let offsets: StaticView<u8, _> = Stream(&[0, 4]).extract((2, ()));
///
/// // Construct a view of 2 little-endian `u16`s at the above byte offsets.
/// let bytes = &[1, 2, 0, 0, 3, 4, 0, 0];
/// let view: OffsetView<_, u16, _> =
///     OffsetView::new(offsets, bytes, Endianness::Little);
///
/// // Extract the bytes for the values in the view.
/// // Note that because the size of the values is unknown, more bytes than
/// // necessary are returned.
/// assert_eq!(view.get(0), Some(&[1, 2, 0, 0, 3, 4, 0, 0][..]));
/// assert_eq!(view.get(1), Some(&[3, 4, 0, 0][..]));
/// assert_eq!(view.get(2), None);
///
/// // Extract the offsets and values in the view.
/// assert_eq!(view.offsets().iter().zip(view.iter()).collect::<Vec<_>>(), &[
///     (0, 0x0201),
///     (4, 0x0403),
/// ]);
/// ```
#[derive(Copy, Clone, Debug)]
pub struct OffsetView<'s, O, T, P> where
    O: View<'s> + Copy,
    O::Item: Extract<'s, O::Parameter> + Offset,
    T: TryExtract<'s, P>,
    P: Copy
{
    offsets: O,
    bytes: &'s [u8],
    parameter: P,
    _marker: PhantomData<*const T>,
}

impl<'s, O, T, P> OffsetView<'s, O, T, P> where
    O: View<'s> + Copy,
    O::Item: Extract<'s, O::Parameter> + Offset,
    T: TryExtract<'s, P>,
    P: Copy
{
    /// Returns a new offset view.
    #[inline]
    pub fn new(offsets: O, bytes: &'s [u8], parameter: P) -> Self {
        Self { offsets, bytes, parameter, _marker: PhantomData }
    }

    /// Returns the byte offsets in this offset view.
    #[inline]
    pub fn offsets(&self) -> &O {
        &self.offsets
    }
}

impl<'s, O, T, P> View<'s> for OffsetView<'s, O, T, P> where
    O: View<'s> + Copy,
    O::Item: Extract<'s, O::Parameter> + Offset,
    T: TryExtract<'s, P>,
    P: Copy
{
    type Parameter = P;
    type Item = T;

    #[inline]
    fn len(&self) -> usize {
        self.offsets.len()
    }

    #[inline]
    fn parameter(&self) -> Self::Parameter {
        self.parameter
    }

    #[inline]
    fn get(&self, index: usize) -> Option<&'s [u8]> {
        let offset = self.offsets.extract(index)?.offset();
        if offset <= self.bytes.len() {
            Some(&self.bytes[offset..])
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use crate::*;
    use super::*;
    use super::super::*;

    #[test]
    fn test_offset_view() {
        let offsets: StaticView<u8, _> = Stream(&[0, 2, 5, 9]).extract((4, ()));
        let bytes = &[1, 2, 3, 4, 0, 5, 6, 0, 0, 7, 8];
        let view: OffsetView<_, u16, _> = OffsetView::new(offsets, bytes, ());

        assert_eq!(view.get(0), Some(&[1, 2, 3, 4, 0, 5, 6, 0, 0, 7, 8][..]));
        assert_eq!(view.get(1), Some(&[3, 4, 0, 5, 6, 0, 0, 7, 8][..]));
        assert_eq!(view.get(2), Some(&[5, 6, 0, 0, 7, 8][..]));
        assert_eq!(view.get(3), Some(&[7, 8][..]));
        assert_eq!(view.get(4), None);

        assert_eq!(view.extract(0), Some(0x0102));
        assert_eq!(view.extract(1), Some(0x0304));
        assert_eq!(view.extract(2), Some(0x0506));
        assert_eq!(view.extract(3), Some(0x0708));
        assert_eq!(view.extract(4), None);
    }
}
