// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Sequences of extractable values.
//!
//! Views are lazily extracted sequences of values. As `tarrasque` does not
//! utilize dynamic memory allocation, views are the primary solution to the
//! problem of extracting dynamically sized sequences of values.

mod dynamic;
mod dynamic_offset;
mod offset;
mod parameter_record;
mod record;
#[path="static.rs"] mod static_;
mod static_offset;

pub use self::dynamic::*;
pub use self::dynamic_offset::*;
pub use self::offset::*;
pub use self::parameter_record::*;
pub use self::record::*;
pub use self::static_::*;
pub use self::static_offset::*;

use core::cmp::{Ordering};
use core::marker::{PhantomData};

use super::{Extract, ExtractResult, Stream, TryExtract};

/// A type containing a sequence of extractable values.
pub trait View<'s> {
    /// The type of the parameter used to extract values from this view.
    type Parameter;
    /// The type of extractable values in this view.
    type Item: TryExtract<'s, Self::Parameter>;

    /// Returns the number of values in this view.
    fn len(&self) -> usize;

    /// Returns whether there are no values in this view.
    #[inline]
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Returns the parameter used to extract values from this view.
    fn parameter(&self) -> Self::Parameter;

    /// Returns the bytes for the value at the supplied index in this view.
    fn get(&self, index: usize) -> Option<&'s [u8]>;

    /// Extracts the value at the supplied index in this view.
    #[inline]
    fn extract(
        &self, index: usize
    ) -> Option<Self::Item> where Self::Item: Extract<'s, Self::Parameter> {
        Some(Stream(self.get(index)?).extract(self.parameter()))
    }

    /// Extracts the value at the supplied index in this view.
    #[inline]
    fn try_extract(
        &self, index: usize
    ) -> Option<ExtractResult<'s, Self::Item>> {
        Some(Stream(self.get(index)?).try_extract(self.parameter()))
    }

    /// Returns an iterator over the infallibly extracted values in this view.
    #[inline]
    fn iter(self) -> ExtractIter<'s, Self> where
        Self: Sized,
        Self::Item: Extract<'s, Self::Parameter>
    {
        let length = self.len();
        ExtractIter { view: self, index: 0, length, _marker: PhantomData }
    }

    /// Returns an iterator over the fallibly extracted values in this view.
    #[inline]
    fn try_iter(self) -> TryExtractIter<'s, Self> where Self: Sized {
        let length = self.len();
        TryExtractIter { view: self, index: 0, length, _marker: PhantomData }
    }

    /// Binary searches this view for the supplied value.
    #[inline]
    fn binary_search(
        &self, value: &Self::Item
    ) -> Option<usize> where
        Self::Item: Extract<'s, Self::Parameter> + Ord
    {
        self.binary_search_by(|v| v.cmp(value)).map(|(i, _)| i)
    }

    /// Binary searches this view using the supplied comparator function.
    #[inline]
    fn binary_search_by(
        &self, mut f: impl FnMut(&Self::Item) -> Ordering
    ) -> Option<(usize, Self::Item)> where
        Self::Item: Extract<'s, Self::Parameter>
    {
        let mut low = 0isize;
        let mut high = self.len() as isize - 1;

        while low <= high {
            let middle = low + ((high - low) / 2);
            let value = self.extract(middle as usize).unwrap();
            match f(&value) {
                Ordering::Equal => return Some((middle as usize, value)),
                Ordering::Less => low = middle + 1,
                Ordering::Greater => high = middle - 1,
            }
        }

        None
    }

    /// Binary searches this view using the supplied key extraction function.
    #[inline]
    fn binary_search_by_key<K>(
        &self, key: &K, mut f: impl FnMut(&Self::Item) -> K
    ) -> Option<(usize, Self::Item)> where
        Self::Item: Extract<'s, Self::Parameter>,
        K: Ord
    {
        self.binary_search_by(|v| f(v).cmp(key))
    }

    /// Binary searches this view for the supplied bytes.
    #[inline]
    fn binary_search_bytes(&self, bytes: &[u8]) -> Option<usize> {
        self.binary_search_bytes_by(|b| b.cmp(bytes)).map(|(i, _)| i)
    }

    /// Binary searches the view using the supplied comparator function.
    #[inline]
    fn binary_search_bytes_by(
        &self, mut f: impl FnMut(&'s [u8]) -> Ordering
    ) -> Option<(usize, &'s [u8])> {
        let mut low = 0isize;
        let mut high = self.len() as isize - 1;

        while low <= high {
            let middle = low + ((high - low) / 2);
            let bytes = self.get(middle as usize).unwrap();
            match f(&bytes) {
                Ordering::Equal => return Some((middle as usize, bytes)),
                Ordering::Less => low = middle + 1,
                Ordering::Greater => high = middle - 1,
            }
        }

        None
    }

    /// Binary searches this view using the supplied key extraction function.
    #[inline]
    fn binary_search_bytes_by_key<K>(
        &self, key: &K, mut f: impl FnMut(&'s [u8]) -> K
    ) -> Option<(usize, &'s [u8])> where K: Ord {
        self.binary_search_bytes_by(|b| f(b).cmp(key))
    }
}

/// An iterator over the infallibly extracted values in a view.
#[derive(Copy, Clone, Debug)]
pub struct ExtractIter<'s, V> where
    V: View<'s>,
    V::Item: Extract<'s, V::Parameter>
{
    view: V,
    index: usize,
    length: usize,
    _marker: PhantomData<&'s ()>,
}

impl<'s, V> Iterator for ExtractIter<'s, V> where
    V: View<'s>,
    V::Item: Extract<'s, V::Parameter>
{
    type Item = V::Item;

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.length - self.index;
        (size, Some(size))
    }

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.length {
            let value = self.view.extract(self.index);
            self.index += 1;
            value
        } else {
            None
        }
    }
}

impl<'s, V> DoubleEndedIterator for ExtractIter<'s, V> where
    V: View<'s>,
    V::Item: Extract<'s, V::Parameter>
{
    #[inline]
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.index < self.length {
            let value = self.view.extract(self.length - 1);
            self.length -= 1;
            value
        } else {
            None
        }
    }
}

impl<'s, V> ExactSizeIterator for ExtractIter<'s, V> where
    V: View<'s>,
    V::Item: Extract<'s, V::Parameter> { }

/// An iterator over the fallibly extracted values in a view.
#[derive(Copy, Clone, Debug)]
pub struct TryExtractIter<'s, V> where V: View<'s> {
    view: V,
    index: usize,
    length: usize,
    _marker: PhantomData<&'s ()>,
}

impl<'s, V> Iterator for TryExtractIter<'s, V> where V: View<'s> {
    type Item = ExtractResult<'s, V::Item>;

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.length - self.index;
        (size, Some(size))
    }

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.length {
            let value = self.view.try_extract(self.index);
            self.index += 1;
            value
        } else {
            None
        }
    }
}

impl<'s, V> DoubleEndedIterator for TryExtractIter<'s, V> where V: View<'s> {
    #[inline]
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.index < self.length {
            let value = self.view.try_extract(self.length - 1);
            self.length -= 1;
            value
        } else {
            None
        }
    }
}

impl<'s, V> ExactSizeIterator for TryExtractIter<'s, V> where V: View<'s> { }

#[cfg(test)]
mod test {
    use crate::*;
    use super::*;

    #[test]
    fn test_view_binary_search() {
        let mut stream = Stream(&[0, 0, 0, 2, 0, 4]);
        let view: StaticView<u16, _> = stream.try_extract((3, ())).unwrap();

        assert_eq!(view.binary_search(&0), Some(0));
        assert_eq!(view.binary_search(&1), None);
        assert_eq!(view.binary_search(&2), Some(1));
        assert_eq!(view.binary_search(&3), None);
        assert_eq!(view.binary_search(&4), Some(2));

        assert_eq!(view.binary_search_bytes(&[0, 0]), Some(0));
        assert_eq!(view.binary_search_bytes(&[0, 1]), None);
        assert_eq!(view.binary_search_bytes(&[0, 2]), Some(1));
        assert_eq!(view.binary_search_bytes(&[0, 3]), None);
        assert_eq!(view.binary_search_bytes(&[0, 4]), Some(2));
    }
}
