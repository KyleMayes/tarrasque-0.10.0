// Copyright 2018 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Static views.

use core::marker::{PhantomData};

use crate::{Extract, ExtractResult, Span, Stream, TryExtract};
use super::{View};

/// A contiguous sequence of same-sized values where the size of the values is
/// known at compile-time.
///
/// # Example
///
/// ```
/// # use tarrasque::{Endianness, Extract, Stream};
/// # use tarrasque::view::{StaticView, View};
/// let mut stream = Stream(&[1, 2, 3, 4]);
///
/// // Extract a view of 2 little-endian `u16`s.
/// let view: StaticView<u16, _> = stream.extract((2, Endianness::Little));
///
/// // Extract the values in the view.
/// assert_eq!(view.extract(0), Some(0x0201));
/// assert_eq!(view.extract(1), Some(0x0403));
/// assert_eq!(view.extract(2), None);
///
/// // Iterate over the values in the view.
/// assert_eq!(view.iter().collect::<Vec<_>>(), &[0x0201, 0x0403]);
/// ```
#[derive(Copy, Clone, Debug)]
pub struct StaticView<'s, T, P> where
    T: TryExtract<'s, P> + Span,
    P: Copy
{
    bytes: &'s [u8],
    length: usize,
    parameter: P,
    _marker: PhantomData<*const T>,
}

impl<'s, T, P> Extract<'s, (usize, P)> for StaticView<'s, T, P> where
    T: TryExtract<'s, P> + Span,
    P: Copy
{
    #[inline]
    fn extract(
        stream: &mut Stream<'s>, (length, parameter): (usize, P)
    ) -> Self {
        let bytes = stream.extract(length * T::SPAN);
        Self { bytes, length, parameter, _marker: PhantomData }
    }
}

impl<'s, T, P> TryExtract<'s, (usize, P)> for StaticView<'s, T, P> where
    T: TryExtract<'s, P> + Span,
    P: Copy
{
    #[inline]
    fn try_extract(
        stream: &mut Stream<'s>, (length, parameter): (usize, P)
    ) -> ExtractResult<'s, Self> {
        let bytes = stream.try_extract(length * T::SPAN)?;
        Ok(Self { bytes, length, parameter, _marker: PhantomData })
    }
}

impl<'s, T, P> View<'s> for StaticView<'s, T, P> where
    T: TryExtract<'s, P> + Span,
    P: Copy
{
    type Parameter = P;
    type Item = T;

    #[inline]
    fn len(&self) -> usize {
        self.length
    }

    #[inline]
    fn parameter(&self) -> Self::Parameter {
        self.parameter
    }

    #[inline]
    fn get(&self, index: usize) -> Option<&'s [u8]> {
        if index < self.length {
            let start = index * T::SPAN;
            let end = start + T::SPAN;
            Some(&self.bytes[start..end])
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use crate::*;
    use crate::ExtractError::*;
    use super::*;

    #[test]
    fn test_extract_static_view() {
        let mut stream = Stream(&[1, 2, 3, 4]);
        let view: StaticView<u16, _> = stream.extract((2, ()));
        assert_eq!(view.extract(0), Some(0x0102));
        assert_eq!(view.extract(1), Some(0x0304));
        assert_eq!(view.extract(2), None);

        let mut iter = view.iter();
        assert_eq!(iter.len(), 2);
        assert_eq!(iter.next(), Some(0x0102));
        assert_eq!(iter.len(), 1);
        assert_eq!(iter.next(), Some(0x0304));
        assert_eq!(iter.len(), 0);
        assert_eq!(iter.next(), None);

        let mut iter = view.iter();
        assert_eq!(iter.len(), 2);
        assert_eq!(iter.next_back(), Some(0x0304));
        assert_eq!(iter.len(), 1);
        assert_eq!(iter.next_back(), Some(0x0102));
        assert_eq!(iter.len(), 0);
        assert_eq!(iter.next_back(), None);
    }

    #[test]
    #[should_panic(expected="insufficient bytes")]
    fn test_extract_static_view_panic() {
        let mut stream = Stream(&[1, 2, 3, 4]);
        let _ = stream.extract::<StaticView<u16, _>, _>((3, ()));
    }

    #[test]
    fn test_try_extract_static_view() {
        let mut stream = Stream(&[1, 2, 3, 4]);
        let view: StaticView<u16, _> = stream.try_extract((2, ())).unwrap();
        assert_eq!(view.try_extract(0), Some(Ok(0x0102)));
        assert_eq!(view.try_extract(1), Some(Ok(0x0304)));
        assert_eq!(view.try_extract(2), None);

        let mut stream = Stream(&[1, 2, 3, 4]);
        match stream.try_extract::<StaticView<u16, _>, _>((3, ())) {
            Err(error) => assert_eq!(error, Insufficient(6)),
            _ => unreachable!(),
        }

        let mut iter = view.try_iter();
        assert_eq!(iter.len(), 2);
        assert_eq!(iter.next(), Some(Ok(0x0102)));
        assert_eq!(iter.len(), 1);
        assert_eq!(iter.next(), Some(Ok(0x0304)));
        assert_eq!(iter.len(), 0);
        assert_eq!(iter.next(), None);

        let mut iter = view.try_iter();
        assert_eq!(iter.len(), 2);
        assert_eq!(iter.next_back(), Some(Ok(0x0304)));
        assert_eq!(iter.len(), 1);
        assert_eq!(iter.next_back(), Some(Ok(0x0102)));
        assert_eq!(iter.len(), 0);
        assert_eq!(iter.next_back(), None);
    }
}
